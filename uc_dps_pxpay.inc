<?php
/**
 * @file
 * Wrapper for PXPay API.
 *
 * Refer Payment Express Ecommerce: PXPay Interface from DPS Payment Express.
 *
 * @author Jonathan Hunt uc_dps_pxpay.inc@huntdesign.co.nz
 */

/**
 * 1. Generate Request
 * If request is valid, return URL to redirect to.
 *
 * @TODO Call this as an array, with default values.
 * As per Commerce DPS module.
 */
function uc_dps_pxpay_generate_request($vars) {
  $default_vars = array(
    'type' => 'Purchase',
    'currency' => 'NZD',
    'reference' => '',
    'email' => '',
    'txn_id' => '',
  );
  $vars = array_merge($default_vars, $vars);

  $xml = '<GenerateRequest>';
  $xml .= '  <PxPayUserId>' . $vars['user_id'] . '</PxPayUserId>';
  $xml .= '  <PxPayKey>' . $vars['key'] . '</PxPayKey>';
  $xml .= '  <TxnType>' . $vars['type'] . '</TxnType>';
  $xml .= '  <CurrencyInput>' . $vars['currency'] . '</CurrencyInput>';

  // Ensure amount is dd.cc
  $vars['amount'] = number_format($vars['amount'], 2, '.', '');
  $xml .= '  <AmountInput>' . $vars['amount'] . '</AmountInput>';
  $xml .= '  <MerchantReference>' . $vars['reference'] . '</MerchantReference>';
  $xml .= '  <EmailAddress>' . $vars['email'] . '</EmailAddress>';
  $xml .= '  <TxnId>' . $vars['txn_id'] . '</TxnId>';
  $xml .= '  <UrlSuccess>' . $vars['url_success'] . '</UrlSuccess>';
  $xml .= '  <UrlFail>' . $vars['url_failure'] . '</UrlFail>';
  $xml .= '</GenerateRequest>';

  if (variable_get('uc_dps_pxpay_log', FALSE)) {
    watchdog('uc_dps_pxpay', 'GenerateRequest: @reference, @xml', array('@reference' => $vars['reference'], '@xml' => $xml), WATCHDOG_DEBUG);
  }

  $url = variable_get('uc_dps_pxpay_server', 'https://sec.paymentexpress.com/pxpay/pxaccess.aspx');
  $response = drupal_http_request($url, array('method' => 'POST', 'data' => $xml));

  if (variable_get('uc_dps_pxpay_log', FALSE)) {
    watchdog(
      'uc_dps_pxpay',
      'GenerateRequest response: @reference, HTTP @code, @xml',
      array(
        '@reference' => $vars['reference'],
        '@code' => $response->code,
        '@xml' => $response->data,
      ),
      WATCHDOG_DEBUG
    );
  }

  // If response from PxPay is ok, extract and return
  // Hosted Payment Page URI to redirect user to.
  if ($response->code == 200) {
    $xml = simplexml_load_string($response->data);
    foreach ($xml->attributes() as $attribute => $value) {
      if ($attribute == 'valid' && $value == '1') {
        return $xml->URI;
      }
      else {
        watchdog(
          'uc_dps_pxpay',
          'GenerateRequest invalid: @reference, XML: @xml',
          array(
            '@reference' => $vars['reference'],
            '@xml' => $response->data,
          ),
          WATCHDOG_ERROR
        );
      }
    }
  }
  else {
    watchdog(
      'uc_dps_pxpay',
      'GenerateRequest: @reference, HTTP @code, XML: @xml',
      array(
        '@reference' => $vars['reference'],
        '@code' => $response->code,
        '@xml' => $response->data,
      ),
      WATCHDOG_ERROR
    );
  }
  return FALSE;
}

/**
 * Generate XML for PxPay Process Response.
 *
 * @param array $vars,
 *   containing
 *   - userid User id assigned by DPS.
 *   - key Merchant account key assigned by DPS.
 *   - response Encryped transaction data sent from DPS.
 *
 * @return array $data Transaction data.
 */
function uc_dps_pxpay_process_response($vars) {
  $xml = '<ProcessResponse>';
  $xml .= '<PxPayUserId>' . $vars['userid'] . '</PxPayUserId>';
  $xml .= '<PxPayKey>' . $vars['key'] . '</PxPayKey>';
  $xml .= '<Response>' . $vars['response'] . '</Response>';
  $xml .= '</ProcessResponse>';

  if (variable_get('uc_dps_pxpay_log', FALSE)) {
    watchdog('uc_dps_pxpay', 'ProcessResponse: @xml', array('@xml' => $xml), WATCHDOG_DEBUG);
  }

  $url = variable_get('uc_dps_pxpay_server', 'https://sec.paymentexpress.com/pxpay/pxaccess.aspx');
  $response = drupal_http_request($url, array('method' => 'POST', 'data' => $xml));

  if (variable_get('uc_dps_pxpay_log', FALSE)) {
    watchdog(
      'uc_dps_pxpay',
      'ProcessResponse response: HTTP @code, @xml',
      array(
        '@code' => $response->code,
        '@xml' => $response->data,
      ),
      WATCHDOG_DEBUG
    );
  }

  // 6. Response XML Document. If response from PxPay is good, extract
  // and return transaction data.
  if ($response->code == 200) {
    $xml = simplexml_load_string($response->data);
    // Build data array.
    foreach ($xml->attributes() as $attribute => $value) {
      if ($attribute == 'valid' && $value == '1') {
        $data = array();
        // Since response XML is only one level deep, simply iterate over
        // XML elements and gather values.
        foreach ($xml->children() as $child) {
          $data[$child->getName()] = (string) $child;
        }
        return $data;
      }
      else {
        // @todo: send to watchdog.
      }
    }
  }
  return FALSE;
}

/**
 * Returns an array of possible currency codes.
 */
function uc_dps_pxpay_currencies() {
  return array(
    'CAD' => 'Canadian Dollar',
    'CHF' => 'Swiss Franc',
    'EUR' => 'Euro',
    'FRF' => 'French Franc',
    'GBP' => 'United Kingdom Pound',
    'HKD' => 'Hong Kong Dollar',
    'JPY' => 'Japanese Yen',
    'NZD' => 'New Zealand Dollar',
    'SGD' => 'Singapore Dollar',
    'USD' => 'United States Dollar',
    'ZAR' => 'Rand',
    'AUD' => 'Australian Dollar',
    'WST' => 'Samoan Tala',
    'VUV' => 'Vanuatu Vatu',
    'TOP' => 'Tongan Pa\'anga',
    'SBD' => 'Solomon Islands Dollar',
    'PNG' => 'Papua New Guinea Kina',
    'MYR' => 'Malaysian Ringgit',
    'KWD' => 'Kuwaiti Dinar',
    'FJD' => 'Fiji Dollar',
  );
}
