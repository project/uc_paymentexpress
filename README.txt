
-- CONTENTS --

 * Summary
 * PX Post vs PX Access
 * Requirements
 * Installation
 * Feedback
 
-- SUMMARY --

Payment Express is a payment processor popular in NZ and Australia.

This module implements two of their payment processing methods - PX Post and PX Access.

For full details, visit http://drupal.org/project/uc_paymentexpress


-- PX POST vs PX ACCESS --

PX Post is a merchant-hosted payment solution. Credit cards will be collected on your website,
so additional security steps are required.

To PX Post this you will need to configure an SSL certificate on your webserver, and will 
likely have to complete additional requirements for verification as a CC processor.

PX Access is a DPS hosted payment solution. Credit cards will be collected on DPS's secure
website, and transaction confirmation/rejection information will be passed back to your cart.

Consider PX Access if you are on a shared server, don't know what PCI-DSS stands for, or want
a simpler solution to integrate.


-- REQUIREMENTS --

 * Ubercart's uc_cart and uc_credit modules.
 * PX Access requires requires cURL.
 * PX Post requires cURL (to 6.x-1.0)
 * PX Post requires SimpleXML (versions above 6.x-1.0)

-- INSTALLATION --



-- FEEDBACK --

Please post feedback at http://drupal.org/project/uc_paymentexpress

Make sure to search the issue queue before creating a new issue!
