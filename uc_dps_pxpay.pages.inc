<?php

/**
 * @file
 * Handle referral from DPS Hosted Payment Page, fetch transaction data
 * and update order status.
 *
 * @author Robbie Mackay <rm@robbiemackay.com>
 *
 * @copyright Robbie MacKay 2009
 */

/**
 * Once payment details have been entered into the PXPay form hosted by DPS,
 * handle a Transaction Response.
 * Refer 4. HTTP Redirect.
 */
function uc_dps_pxpay_complete($order_id = 0) {
  if (!($order = uc_order_load($order_id))) {
    drupal_goto('cart');
  }

  // 5. Process response.
  module_load_include('inc', 'uc_dps_pxpay');
  $vars = array(
    'userid'   => variable_get('uc_dps_pxpay_userid', ''),
    'key'      => variable_get('uc_dps_pxpay_key', ''),
    'response' => $_GET['result'],
  );
  $data = uc_dps_pxpay_process_response($vars);

  // Populate order data with values from API.
  if ($data === FALSE) {
    drupal_set_message(t('Your DPS payment was declined. Please feel free to continue shopping or contact us for assistance.'));
    drupal_goto(variable_get('uc_dps_pxpay_cancel_return_url', 'cart'));
  }
  else {
    // =1 when request succeeds.
    $order->data['payment_details']['pxpay_Success'] = $data['Success'];
    $payment_amount
      = $order->data['payment_details']['pxpay_AmountSettlement']
      = $data['AmountSettlement'];
    $payment_currency
      = $order->data['payment_details']['pxpay_Currency']
      = $data['CurrencySettlement'];
    // From bank.
    $order->data['payment_details']['pxpay_AuthCode'] = $data['AuthCode'];
    // E.g. "Visa".
    $order->data['payment_details']['pxpay_CardName'] = $data['CardName'];
    // Truncated card number.
    $order->data['payment_details']['pxpay_CardNumber'] = $data['CardNumber'];
    $order->data['payment_details']['pxpay_CardHolderName'] = $data['CardHolderName'];
    $order->data['payment_details']['pxpay_DateExpiry'] = $data['DateExpiry'];
    $order->data['payment_details']['pxpay_DpsTxnRef'] = $data['DpsTxnRef'];
    $order->data['payment_details']['pxpay_TxnType'] = $data['TxnType'];
    // The IP address of the user who submitted the transaction.
    $order->data['payment_details']['pxpay_ClientInfo'] = $data['ClientInfo'];
    $order->data['payment_details']['pxpay_TxnId'] = $data['TxnId'];
    // $order->data['payment_details']['pxpay_EmailAddress']
    // = $data['EmailAddress'];
    $order->data['payment_details']['pxpay_MerchantReference']
      = $data['MerchantReference'];
    // e.g. APPROVED.
    $order->data['payment_details']['pxpay_ResponseText'] = $data['ResponseText'];
    // e.g. 1.23.
    $order->data['payment_details']['pxpay_AmountSettlement'] = $data['AmountSettlement'];
    // e.g. NZD.
    $order->data['payment_details']['pxpay_CurrencySettlement'] = $data['CurrencySettlement'];

    // Check transaction id matches.
    if ($order->data['pxpay_txnid'] != $data['TxnId']) {
      watchdog(
        'uc_cart',
        'DPS PXPay details does not match order details! Cart order ID: @cart_order',
        array('@cart_order' => $order->order_id),
        WATCHDOG_ERROR
      );

      uc_order_save($order);
      uc_order_update_status($order->order_id, 'pxpay_onhold');
      uc_order_comment_save($order->order_id, 0, t('DPS PXPay payment details do not match order.'), 'admin', 'pxpay_onhold');
      uc_order_comment_save(
        $order->order_id,
        0,
        t('DPS PXPay payment error - contact us for assistance.'),
        'order',
        'pxpay_onhold'
      );

      drupal_set_message(
        t(
          'The details of your DPS PXPay payment do not match your order. Please contact us for assistance. Reference Order Id: @orderid',
          array('@orderid' => $order->order_id)
        )
      );
      drupal_goto(variable_get('uc_dps_pxpay_cancel_return_url', 'cart'));
    }
    else {
      // Store payment data against this order.
      uc_order_save($order);
    }

    // Generate payment comment for duplicate check.
    $comment = t('DPS PXPay Auth code: @auth_code, Transaction ID: @txn_id', array(
      '@auth_code' => $order->data['payment_details']['pxpay_AuthCode'],
      '@txn_id' => $order->data['payment_details']['pxpay_DpsTxnRef'],
    ));

    $payments = uc_payment_load_payments($order_id);

    // Check for duplicate transaction. DPS sends FPRN and also returns browser
    // with data to validate transaction, so we are likely to see both.
    //
    // Both notifications should set the payment for the order, but only once.
    //
    // The browser return should fire uc_cart_complete_sale() to log in if reqd.
    $duplicate_flag = FALSE;
    if (!empty($payments)) {
      foreach ($payments as $payment) {
        if ($payment->comment == $comment) {
          $duplicate_flag = TRUE;
        }
      }
    }

    if (!$duplicate_flag) {
      uc_order_save($order);

      // Move an order's status from "In Checkout" to "Payment Received".
      if (isset($order->status) && $order->status == 'in_checkout') {
        uc_order_update_status($order->order_id, 'payment_received');
      }

      uc_payment_enter($order_id, 'uc_dps_pxpay_dps', $payment_amount, $order->uid, $order->data['payment_details'], $comment);
      uc_order_comment_save($order->order_id, 0, t('DPS PXPay payment successful'), 'order', 'payment_received');

      uc_order_comment_save($order_id, 0, t('Payment of @amount @currency submitted via DPS PXPay (@txn_id).', array(
        '@amount' => uc_currency_format($payment_amount, FALSE),
        '@currency' => $payment_currency,
        '@txn_id' => $order->data['payment_details']['pxpay_DpsTxnRef'],
      )), 'order', 'payment_received');

      uc_order_comment_save($order_id, 0, t('PXPay reported a payment of @amount @currency.', array(
        '@amount' => uc_currency_format($payment_amount, FALSE),
        '@currency' => $payment_currency,
      )));
    }

    if (isset($_SESSION['cart_order']) && intval($_SESSION['cart_order']) == $order_id) {
      $build = uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE));
    }

    // Refer uc_cart.pages.inc.
    if (isset($_SESSION['uc_checkout'][$order->order_id]['do_review'])) {
      unset($_SESSION['uc_checkout'][$order->order_id]['do_review']);
    }
    $_SESSION['uc_checkout'][$order->order_id]['do_complete'] = TRUE;
    drupal_set_message(t('Your DPS payment was successful.'));

    $page = variable_get('uc_cart_checkout_complete_page', '');
    if (!empty($page)) {
      drupal_goto($page);
    }
    elseif (!empty($build)) {
      return $build;
    }
  }
}

/**
 * Handle cancelled transaction.
 */
function uc_dps_pxpay_cancel() {
  if (isset($_SESSION['cart_order'])) {
    unset($_SESSION['cart_order']);
  }
  drupal_set_message(t('Your DPS payment was cancelled. Please feel free to continue shopping or contact us for assistance.'));
  drupal_goto(variable_get('uc_dps_pxpay_cancel_return_url', 'cart'));
}
